%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Dragging
  m_Mask: 01000000010000000100000001000000010000000000000001000000000000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Bind_Hips
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_LeftUpLeg
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_LeftUpLeg/Bind_LeftLeg/Bind_LeftFoot/Bind_LeftToeBase/Bind_LeftToe_End
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_RightUpLeg
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_RightUpLeg/Bind_RightLeg/Bind_RightFoot/Bind_RightToeBase/Bind_RightToe_End
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_LeftShoulder/Bind_LeftArm/Bind_LeftForeArm/Bind_LeftHand
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_Neck/Bind_Head/Bind_HeadTop_End
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm
    m_Weight: 1
  - m_Path: Bind_Hips/Bind_Spine/Bind_Spine1/Bind_Spine2/Bind_RightShoulder/Bind_RightArm/Bind_RightForeArm/Bind_RightHand
    m_Weight: 1
  - m_Path: Mesh_GRP
    m_Weight: 1
  - m_Path: Mesh_GRP/Mesh_Big_head_cleaner
    m_Weight: 1
  - m_Path: Mesh_GRP/Mesh_Big_head_police
    m_Weight: 1
