using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndDoor : Interactable
{
    [Header("door stuff: ")]
    [SerializeField] Collider col;
    [SerializeField] Renderer doorRen;
    [SerializeField] ParticleSystem arrowParticle;
    [SerializeField] Animator doorAnimator;

    readonly string OPEN = "open";
    
    void Start()
    {
        col.enabled = false;
        doorRen.enabled = false;
        gameController = GameController.GetController();

        //Vector3 dir = (arrowParticle.transform.position - transform.position).normalized;
        //float angle = Vector3.Angle(Vector3.right, dir);
        //Debug.LogError("angle to door : " + angle);
        //Debug.DrawLine(arrowParticle.transform.position, dir * 20f, Color.green, 55f);
        //var main = arrowParticle.main;
        //main.startRotation = Mathf.Deg2Rad * angle;
        //arrowParticle.Play();
    }

    public override void Interact()
    {
        base.Interact();
        //otherCollider.GetComponent<PlayerController>().PickUp(transform);
        gameController.LevelComplete();
        transform.GetComponent<Collider>().enabled = false;
        doorRen.enabled = false;
        if (doorAnimator)
            doorAnimator.SetTrigger(OPEN);
    }
    public void OpenGate()
    {
        col.enabled = true;
        doorRen.enabled = true;
        arrowParticle.Play();
    }
}
