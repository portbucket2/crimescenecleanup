using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorScanner : Interactable
{
    [SerializeField] Vector3 rot;
    GameController gameController;
    void Start()
    {
        gameController = GameController.GetController();
    }
    public override void Interact()
    {
        base.Interact();
        PlayerController pl = otherCollider.GetComponent<PlayerController>();
        if (pl.IsOccupiedOnly())
        {
            gameController.TaskComplete(CleanerTask.UNLOCKDOOR);
            GetComponent<Collider>().enabled = false;
            transform.DORotate(rot, 0.5f);
        }
    }
}
