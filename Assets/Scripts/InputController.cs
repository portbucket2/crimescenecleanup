using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [Header("Debug :")]
    [SerializeField] Joystick joystick;
    [SerializeField] bool inputEnabled = true;
    float horizontal;
    float vertical;
    GameController gameController;
    PlayerController player;
    void Start()
    {
        gameController = GameController.GetController();
        joystick = gameController.GetJoystick();
        player = gameController.GetPlayerController();
    }

    // Update is called once per frame
    void Update()
    {
        if (!inputEnabled)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            player.Moveable();
        }
        if (Input.GetMouseButton(0))
        {
            horizontal = joystick.Horizontal;
            vertical = joystick.Vertical;

            player.Move(horizontal, vertical);
        }
        if (Input.GetMouseButtonUp(0))
        {
            player.Stop();
        }
    }

    public void InputEnabled(bool val)
    {
        inputEnabled = val;
        joystick.gameObject.SetActive(val);
    }
}
