using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpInteractable : Interactable
{
    [SerializeField] ParticleSystem glowParticle;
    [SerializeField] ParticleSystem followUpParticle;
    void Start()
    {

    }
    //public Transform GetRB() { return targetRb; }
    public override void Interact()
    {
        base.Interact();
        PlayerController pl = otherCollider.GetComponent<PlayerController>();

        //GetComponent<Collider>().enabled = false;
        if (pl.IsOccupied())
        {
            pl.DropBody(transform);

        }else
        {
            pl.PickUp(transform, followUpParticle.transform);
            glowParticle.Stop();
            followUpParticle.Play();
        }
        

        
    }
    public void ReEnable()
    {
        GetComponent<Collider>().enabled = true;
    }
    public Vector3 NextTargetLocation()
    {
        return followUpParticle.transform.position;
    }
}
