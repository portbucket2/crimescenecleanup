using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolHolder : Interactable
{
    [SerializeField] Transform tool;
    [SerializeField] ParticleSystem glowParticle;
    [SerializeField] ParticleSystem followupParticle;
    [Header("Debug: ")]
    [SerializeField] bool isHolding = true;
    [SerializeField] PlayerController pl;
    Vector3 toolPos;
    void Start()
    {
        toolPos = tool.position;
        glowParticle.Play();
    }

    public override void Interact()
    {
        base.Interact();
        pl = otherCollider.GetComponent<PlayerController>();
        if (pl.IsOccupiedOnly())
            return;

        if (pl.IsMoppingOnly())
        {
            Transform tr = pl.DropTool();
            if (tr)
            {
                //tr.position = toolPos;
                //tr.SetParent(transform);
                tool.gameObject.SetActive(true);
                isHolding = true;
            }
            glowParticle.Stop();
            followupParticle.Stop();
        }
        else
        {
            pl.PickUpTool(tool, this);
            tool.gameObject.SetActive(false);
            isHolding = false;
            glowParticle.Stop();
            followupParticle.Play();
        }
        
    }
    public void ReGlow()
    {
        glowParticle.Play();
    }
    public Transform FollowUpTransform()
    {
        return followupParticle.transform;
    }
}
