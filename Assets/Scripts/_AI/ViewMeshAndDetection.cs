using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewMeshAndDetection : MonoBehaviour
{
    [SerializeField] float viewRadius;
    [Range(0, 360)]
    [SerializeField] float viewAngle;
    [SerializeField] LayerMask playerMask;
    [SerializeField] float detectionDelay;
    [SerializeField] float meshResolution;

    [SerializeField] MeshFilter viewMeshFilter;
    [SerializeField] Material greenMat;
    [SerializeField] Material redMat;
    Mesh viewMesh;
    MeshRenderer rend;
    PetrollingGuard guard;

    float delayCount = 0;
    readonly string PLAYER = "Player";
    GameController gameController;
    Transform player;
    void Start()
    {
        gameController = GameController.GetController();
        player = gameController.GetPlayerController().transform;
        rend = viewMeshFilter.GetComponent<MeshRenderer>();
        guard = transform.GetComponent<PetrollingGuard>();
        viewMesh = new Mesh();
        viewMesh.name = "View Mesh";
        viewMeshFilter.mesh = viewMesh;
    }
    private void Update()
    {
        FindPlayer();
    }
    private void LateUpdate()
    {
        DrawFieldOfView();
    }
    void FindPlayer()
    {
        Vector3 dirToTarget = (player.position - transform.position).normalized;
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up, dirToTarget, out hit, 100))
        {
            //Debug.DrawLine(transform.position, hit.point, Color.green);
            if (hit.transform.CompareTag(PLAYER))
            {
                // player detected and nothing in between
                //Debug.DrawLine(transform.position, hit.point, Color.cyan);
                
                if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
                {

                    if (Vector3.Distance(transform.position, player.position) < viewRadius)
                    {
                        rend.material = redMat;
                        guard.PlayerDetected(hit.transform);
                        
                    }else
                        rend.material = greenMat;
                }
                else
                {
                    rend.material = greenMat;
                }
            }
            else
            {
                rend.material = greenMat;
                guard.UndetectPlayer();
            }
        }
        
        
        //-----------old-------way-----
        
            
        //Vector3 dirToTarget = (player.position - transform.position).normalized;
        //if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
        //{
        //    RaycastHit hit;
        //    if (Physics.Raycast(transform.position, dirToTarget, out hit, 100))
        //    {
        //        Debug.DrawLine(transform.position, hit.point, Color.grey);
        //        if (hit.transform.CompareTag(PLAYER))
        //        {
        //            // player detected and nothing in between
        //            rend.material = redMat;
        //            guard.PlayerDetected(hit.transform);
                    
        //        }
        //        else
        //        {
        //            rend.material = greenMat;
        //            guard.UndetectPlayer();
        //        }                    
        //    }
        //}
        //else
        //{
        //    rend.material = greenMat;
        //    //return;
        //}
    }
    void FindVisibleTarget()
    {
        Collider[] targetsInView = Physics.OverlapSphere(transform.position, viewRadius, playerMask);

        for (int i = 0; i < targetsInView.Length; i++)
        {
            Transform target = targetsInView[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float distance = Vector3.Distance(transform.position, target.position);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, dirToTarget, out hit, distance))
                {
                    if (hit.transform.CompareTag(PLAYER))
                    {
                        Debug.DrawLine(transform.position, hit.point, Color.red, 0.3f);
                        rend.material = redMat;
                        Debug.LogError("Detecting!!  true");
                    }
                }
                else
                {
                    rend.material = greenMat;
                    Debug.LogError("Detecting!!  false");
                    Debug.DrawLine(transform.position, hit.point, Color.green, 0.3f);
                }
            }
        }
    }

    private Vector3 DirFromAngle(float angleInDegree, bool isGlobalAngle)
    {
        if (isGlobalAngle)
        {
            angleInDegree += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegree * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegree * Mathf.Deg2Rad));

    }

    void DrawFieldOfView()
    {
        int stepCount = Mathf.RoundToInt(viewAngle * meshResolution);
        float stepAngleSize = viewAngle / stepCount;

        List<Vector3> viewPoints = new List<Vector3>();
        for (int i = 0; i < stepCount; i++)
        {
            float angle = transform.localEulerAngles.y - (viewAngle / 2f) + (stepAngleSize * i);
            //Debug.DrawLine(transform.position, transform.position + DirFromAngle(angle, true) * viewRadius, Color.cyan);
            ViewCastInfo newViewCast = ViewCast(angle);
            viewPoints.Add(newViewCast.point);
        }

        // triangle part
        int vertexCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }
        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = DirFromAngle(globalAngle, false);
        RaycastHit hit;

        if (Physics.Raycast(transform.position, dir, out hit, viewRadius))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, transform.position + dir * viewRadius, viewRadius, globalAngle);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, transform.position + DirFromAngle(transform.eulerAngles.y - viewAngle / 2, false) * viewRadius);
        Gizmos.DrawLine(transform.position, transform.position + DirFromAngle(transform.eulerAngles.y + viewAngle / 2, false) * viewRadius);

    }
}
public struct ViewCastInfo
{
    public bool hit;
    public Vector3 point;
    public float dst;
    public float angle;

    public ViewCastInfo(bool _hit, Vector3 _point, float _dst, float _angle)
    {
        hit = _hit;
        point = _point;
        dst = _dst;
        angle = _angle;
    }
}
