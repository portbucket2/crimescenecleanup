using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PetrollingGuard : MonoBehaviour
{
    [SerializeField] Animator animator;
    [SerializeField] ParticleSystem hitParticle;
    [SerializeField] float petrolSpeed;
    [SerializeField] float captureSpeed;

    [SerializeField] List<Transform> wayPoints;

    [Header("debug: ")]
    [SerializeField] NavMeshAgent agent;
    [SerializeField] bool followingWaypoint = true;
    [SerializeField] bool attackingMode = false;
    [SerializeField] Transform player;
    int maxPoints;
    int waypointCount = 0;

    readonly string SPEED = "speed";
    readonly string HIT = "hit";
    readonly string ALARMED = "alarmed";
    readonly string PLAYER = "Player";
    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITSMALL = new WaitForSeconds(0.2f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);
    WaitForSeconds WAITONE = new WaitForSeconds(1f);

    GameController gameController;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    private void Start()
    {
        gameController = GameController.GetController();
        maxPoints = wayPoints.Count;
        
    }
    private void Update()
    {
        animator.SetFloat(SPEED, agent.velocity.magnitude);



        if (followingWaypoint && Vector3.Distance(agent.destination, transform.position) < 0.10f)
        {
            agent.SetDestination(transform.position);
            MoveToNextWayPoint();            
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            PlayerDetected(other.transform);
        }
    }
    private void MoveToNextWayPoint()
    {
        if (waypointCount >= maxPoints - 1) 
        {
            waypointCount = 0;
        }
        else
        {
            waypointCount++;
        }
        //Debug.LogError("moving next: way: " + waypointCount);
        agent.speed = petrolSpeed;
        agent.SetDestination(wayPoints[waypointCount].position);
    }
    IEnumerator PlayerDetectedRoutine()
    {
        agent.SetDestination(transform.position);
        agent.transform.LookAt(player);
        animator.SetTrigger(ALARMED);
        yield return WAITHALF;
        //agent.speed = captureSpeed;
        agent.SetDestination(player.position);
        while (Vector3.Distance(agent.destination, transform.position) > 2f)
        {
            agent.SetDestination(player.position);
            yield return WAITSMALL;
        }
        yield return ENDOFFRAME;
        agent.SetDestination(transform.position);
        animator.SetTrigger(HIT);
        hitParticle.Play();
        gameController.LevelFailed();
    }

    public void PlayerDetected(Transform _player)
    {
        if (!attackingMode)
        {
            //Debug.LogError("player found!!");
            attackingMode = true;
            player = _player;
            followingWaypoint = false;
            agent.speed = captureSpeed;
            StartCoroutine(PlayerDetectedRoutine());
        }
    }
    public void UndetectPlayer()
    {
        if (attackingMode)
        {
            //Debug.LogError("player gone!!");
            player = null;
            attackingMode = false;
            agent.speed = petrolSpeed;
            StopAllCoroutines();
            followingWaypoint = true;
            agent.SetDestination(wayPoints[waypointCount].position);
        }
    }
}
