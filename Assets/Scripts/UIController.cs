using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject levelCompletePanel;
    [SerializeField] Button nextButton;

    [SerializeField] GameObject levelFailedPanel;
    [SerializeField] Button retryButton;

    [SerializeField] Joystick joystick;
    [SerializeField] GameObject levelStartpanel;
    [SerializeField] Button startButton;
    [SerializeField] Transform tasksUIPanel;
    [SerializeField] GameObject taskParent;
    [SerializeField] TextMeshProUGUI leveltext;
    [SerializeField] TutorialController tutorial;
    [Header("Debug:  ")]
    [SerializeField] List<TaskParentUI> taskParents;
    [SerializeField] List<CleanerTask> taskList;

    bool tutorialShown = false;

    GameManager gameManager;
    AnalyticsController analytics;
    GameController gameController;
    void Start()
    {
        analytics = AnalyticsController.GetController();
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();

        tutorial.gameObject.SetActive(false);
        tutorialShown = gameManager.GetDataManager().GetGamePlayer.handTutorialShown;

        if (gameManager.IsGameOpeningLevel())
        {
            gameController.GetInputController().InputEnabled(false);
            levelStartpanel.SetActive(true);
        }
        else
        {
            gameController.GetInputController().InputEnabled(true);
            levelStartpanel.SetActive(false);
        }
        
        nextButton.onClick.AddListener(delegate
        {
            analytics.LevelCompleted();
            gameManager.GotoNextStage();
        });
        retryButton.onClick.AddListener(delegate
        {
            analytics.LevelRestart();
            gameManager.ResetStage();
        });
        startButton.onClick.AddListener(delegate
        {
            levelStartpanel.SetActive(false);
            gameController.GetInputController().InputEnabled(true);
            gameManager.OpeningLevelComplete();

            if (!tutorialShown)
            {
                tutorial.gameObject.SetActive(true);
            }
            else
                tutorial.gameObject.SetActive(false);
        });
        leveltext.text = "LEVEL " + (gameManager.GetDataManager().GetGamePlayer.levelsCompleted);

        

    }
    public Joystick GetJoystick() { return joystick; }
    public void LevelComplete()
    {
        levelCompletePanel.SetActive(true);
    }
    public void LevelFailed()
    {
        levelFailedPanel.SetActive(true);
    }

    public void CreateTaskList(List<CleanerTask> list)
    {
        taskParents = new List<TaskParentUI>();
             
        taskList = new List<CleanerTask>();
        taskList = list;
        int max = taskList.Count;
        for (int i = 0; i < max; i++)
        {
            GameObject gg =  Instantiate(taskParent, tasksUIPanel);
            TaskParentUI tt = gg.GetComponent<TaskParentUI>();
            tt.SetTaskImage(taskList[i]);
            taskParents.Add(tt);
        }

        GameObject ex = Instantiate(taskParent, tasksUIPanel);
        TaskParentUI ext = ex.GetComponent<TaskParentUI>();
        ext.SetExitDoor();
    }

    public void CheckTaskComplete(CleanerTask _task)
    {
        int max = taskParents.Count;
        for (int i = 0; i < max; i++)
        {
            if(taskParents[i].CompletedTaskCheck( _task ) )
            {
                return;
            }
        }
    }
    public TutorialController GetTutorial()
    {
        return tutorial;
    }
}
