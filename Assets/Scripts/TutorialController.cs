using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class TutorialController : MonoBehaviour
{
    [SerializeField] Camera maincam;
    [SerializeField] Transform tutHand;
    [SerializeField] TextMeshProUGUI tutText;
    [SerializeField] TextMeshProUGUI headline;
    [SerializeField] Transform tut1Target;
    [SerializeField] Transform tut2Target;

    [SerializeField] Vector2 offset;


    Vector2 pos;
    bool showingHand1 = false;
    bool showingHand2 = false;
    Vector2 scrPoint;

    void Start()
    {
        ShowTut1();
    }

    private void Update()
    {
        if (showingHand1)
        {
            tutHand.position = maincam.WorldToScreenPoint(tut1Target.position);
        }else if (showingHand2)
        {
            tutHand.position = maincam.WorldToScreenPoint(tut2Target.position);
        }
        else
        {

        }
    }
    public void ShowTut1()
    {
        if (!tut1Target)
            return;

        scrPoint = maincam.WorldToScreenPoint(tut1Target.position);
        
        tutText.text = "PICK UP";

        showingHand1 = true;
        showingHand2 = false;
    }
    public void ShowTut2()
    {
        if (!tut2Target)
            return;

        scrPoint = maincam.WorldToScreenPoint(tut2Target.position);

        tutText.text = "DISPOSE HERE";

        showingHand1 = false;
        showingHand2 = true;
    }
    public void StopTutorial()
    {
        showingHand1 = false;
        showingHand2 = false;
        tutHand.gameObject.SetActive(false);
        headline.text = "EXIT LEVEL!!";
    }
}
