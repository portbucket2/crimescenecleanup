using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFinder : MonoBehaviour
{
    [SerializeField] LineRenderer ln;
    [Range(0f,1f)]
    [SerializeField] float updateDilay;
    float elapsed = 0.0f;
    [Header("Debug :")]
    [SerializeField] Transform target;
    private NavMeshPath path;

    bool isCalculating = false;

    void Start()
    {
        ln.gameObject.SetActive(false);
        path = new NavMeshPath();
        elapsed = 0.0f;
    }
    private void LateUpdate()
    {
        if (!isCalculating)
            return;

        elapsed += Time.deltaTime;
        if (elapsed > updateDilay)
        {
            elapsed =0;
            DisplayPath();
        }
        
    }
    public void ShowPath( Transform destination)
    {
        target = destination;
        isCalculating = true;
    }
    public void Resetpath()
    {
        ln.gameObject.SetActive(false);
        isCalculating = false;
    }
    void DisplayPath()
    {
        //Debug.LogError("path finder called");
        
        if (NavMesh.CalculatePath(transform.position, target.position, NavMesh.AllAreas, path))
        {
            //Debug.LogError("total corners: " + path.corners.Length);
            ln.gameObject.SetActive(true);
            Vector3[] corners = path.corners;
            ln.SetPositions(corners);
        }
        else
        {
            Debug.LogError("no path found");
            ln.gameObject.SetActive(false);
        }
    }
}
