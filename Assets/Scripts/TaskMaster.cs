using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskMaster : MonoBehaviour
{
    [Header("Set tasks manually to count them : ")]
    [SerializeField] List<CleanerTask> tasks;
    [Header("Debug: ")]
    [SerializeField] int taskCompleted = 0;
    [SerializeField] int totalTask = 0;
    [SerializeField] Dictionary<CleanerTask, bool> taskList;

    GameController gameController;

    void Start()
    {
        gameController = GameController.GetController();
        totalTask = tasks.Count;
        taskList = new Dictionary<CleanerTask, bool>();
        for (int i = 0; i < totalTask; i++)
        {
            taskList.Add(tasks[i], false);
        }
        gameController.GetUI().CreateTaskList(tasks);
    }
    public void TaskComplete(CleanerTask _task)
    {
        for (int i = 0; i < totalTask; i++)
        {
            if (taskList.ContainsKey(_task) )
            {
                if (!taskList[_task])
                {
                    taskList[_task] = true;
                    gameController.GetUI().CheckTaskComplete(_task);
                    taskCompleted++;
                }
            }
        }
        CheckLevelCompletion();
    }
    void CheckLevelCompletion()
    {
        if (taskCompleted >= totalTask)
        {
            Debug.Log("level Complete!!");
            gameController.AllTasksComplete();
        }
    }
    
}
public enum CleanerTask {
    REMOVEBODY,
    CLEANBLOOD,
    CLEANWEAPON,
    UNLOCKDOOR
}
