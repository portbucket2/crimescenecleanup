﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    [SerializeField] bool tutorialShown = false;

    [Header("For Debugging")]
    public int levelcount = 2;
    public int totalScenes = 0;
    public int levelProgressionCount = 1;
    public int lastLevelPlayed = 1;
    public bool isGameOpeningLevel = true;
    [SerializeField] DataManager dataManager;
    [SerializeField] GamePlayer gamePlayer;
    [SerializeField] GameDataSheet dataSheet;

    

    private void Awake()
    {
        gameManager = this;
        DontDestroyOnLoad(this.gameObject);
        dataManager = GetComponent<DataManager>();
        gamePlayer = new GamePlayer();
    }
    void Start()
    {
        totalScenes = SceneManager.sceneCountInBuildSettings;
        
        gamePlayer = dataManager.GetGamePlayer;
        if (gamePlayer.handTutorialShown)
        {
            TutorialSeen();
        }
        //Load last played level
        lastLevelPlayed = gamePlayer.lastPlayedLevel;
        levelcount = gamePlayer.lastPlayedLevel;
        //Debug.Log("level count : " + levelcount);
        if (levelcount > 1)
            SceneManager.LoadScene(levelcount);
        else
        {
            levelcount = 2;
            SceneManager.LoadScene(levelcount);
        }
    }

    public static GameManager GetManager()
    {
        return gameManager;
    }
    public void GotoNextStage()
    {

        //if (levelcount < dataSheet.levelDatas.Count - 1)  
        if (levelcount < totalScenes - 1)  
            levelcount++;
        else
            levelcount = 2;

        SceneManager.LoadScene(levelcount);

        levelProgressionCount++;
    }
    public int GetLevelProgressionCount()
    {
        return levelProgressionCount;
    }
    public void ResetStage()
    {
        SceneManager.LoadScene(levelcount);
    }
    public int GetlevelCount()
    {
        return levelcount;
    }
    public bool TutorialAlreadySeen()
    {
        return tutorialShown;
    }
    public void TutorialSeen()
    {
        tutorialShown = true;
    }
    public DataManager GetDataManager()
    {
        return dataManager;
    }
    public GameDataSheet GetDataSheet()
    {
        return dataSheet;
    }
    public bool IsGameOpeningLevel()
    {
        return isGameOpeningLevel;
    }
    public void OpeningLevelComplete()
    {
        isGameOpeningLevel = false;
    }
}
