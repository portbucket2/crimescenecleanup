﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios.Suite.Analytics;
using LionStudios.Suite.Debugging;
//using com.faithstudio.SDK;
//using GameAnalyticsSDK;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController analyticsController;

    int levelcount = 1;
    int attempt = 1;
    GameManager gameManager;
    private void Awake()
    {
        levelcount = 1;
        analyticsController = this;
        gameManager = transform.GetComponent<GameManager>();
    }
    private void Start()
    {
        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
        //Debug.LogError("level count in analytics: " + levelcount);
        //GameAnalytics.Initialize();
        LionAnalytics.GameStart();
    }

    public static AnalyticsController GetController()
    {
        return analyticsController;
    }

    public void LevelStarted()
    {
        SaveGame();

        levelcount = gameManager.GetDataManager().GetGamePlayer.levelsCompleted;
        
        Debug.LogError("level start in analytics: " + levelcount);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "World01", "Level " + levelcount);
        
        LionDebugger.Hide();
        LionAnalytics.LevelStart(levelcount, attempt);
    }
    public void LevelCompleted()
    {
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "World01", "Level " + levelcount);
        
        attempt = 1;
        
        LionAnalytics.LevelComplete(levelcount, attempt);
        Debug.LogError("level complete in analytics: " + levelcount);
        levelcount++;
        SaveGame();
        
    }
    public void LevelFailed()
    {
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "World01", "Level " + levelcount);
        LionAnalytics.LevelFail(levelcount, attempt);
        attempt++;
    }
    public void LevelRestart()
    {
        //LionAnalytics.LevelRestart(levelcount, attempt);
    }
    public void SaveGame()
    {
        GamePlayer gp = new GamePlayer();
        gp.name = "";
        gp.id = 1;
        gp.levelsCompleted = levelcount;
        gp.totalCoins = 0;// GameController.GetController().GetTotalCoins();
        gp.lastPlayedLevel = gameManager.GetlevelCount();
        gp.handTutorialShown = true;

        gameManager.GetDataManager().SetGameplayerData(gp);
    }

}
