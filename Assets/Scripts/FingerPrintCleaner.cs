using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FingerPrintCleaner : MonoBehaviour
{
    [SerializeField] Transform fingerPrint;
    [SerializeField] Camera camera;
    [SerializeField] Material cleanMat;
    [Header("Debug: ")]
    [SerializeField] bool isErasing = false;
    [SerializeField] MeshRenderer rend;
    [SerializeField] Texture2D copyTex;
    [SerializeField] int blockSize;
    [SerializeField] Color color;
    [Range(0f,1f)]
    [SerializeField] float erasePercent;

    Transform cameraTransform;

    readonly string TopTex = "Top_Texture";
    readonly string PRINT = "print";
    GameController gameController;
    void Start()
    {
        camera.enabled = false;
        gameController = GameController.GetController();
        cameraTransform = camera.transform;
        rend = fingerPrint.GetComponent<MeshRenderer>();
        Texture2D tex = rend.material.mainTexture as Texture2D;
        copyTex = new Texture2D(tex.width, tex.height);
        copyTex.SetPixels(tex.GetPixels());
        color = copyTex.GetPixel(0, 0, 0);
        copyTex.Apply();
        rend.material.mainTexture = copyTex;// .SetTexture(TopTex, copyTex);
        CheckCleningPercentage(tex);
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && isErasing)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition); // new Ray(toolPosition.position, Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 200f))
            {
                if (hit.transform.CompareTag(PRINT))
                {
                    Debug.DrawLine(ray.origin, hit.point, Color.green);
                    //ln.SetPosition(0, camera.transform.position + Vector3.back);
                    //ln.SetPosition(1, hit.point);
                    Renderer rend = hit.transform.GetComponent<Renderer>();
                    MeshCollider meshCollider = hit.collider as MeshCollider;

                    if (rend == null || meshCollider == null)
                        return;

                    //--------------------
                    //Texture2D tex = rend.material.GetTexture(TopTex) as Texture2D;
                    Texture2D tex = rend.material.mainTexture as Texture2D;
                    Vector2 pixelUV = hit.textureCoord;
                    pixelUV.x *= tex.width;
                    pixelUV.y *= tex.height;

                    int max = blockSize * blockSize;
                    Color[] colors = new Color[max];
                    for (int i = 0; i < max; i++)
                    {
                        colors[i] = color;
                    }
                    tex.SetPixels((int)pixelUV.x, (int)pixelUV.y, blockSize, blockSize, colors);

                    CheckCleningPercentage(tex);
                    //tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, aColor);
                    tex.Apply();
                }
            }
        }
    }

    void CheckCleningPercentage(Texture2D _tex)
    {
        Color[] colors = _tex.GetPixels(0);
        int max = colors.Length;
        int count = 0;
        for (int i = 0; i < max; i++)
        {
            if (colors[i].Equals(color)) 
                count++;
        }
        float perc = (float)count / max;
        Debug.Log("Perc: " + perc);
        if (perc > erasePercent)
        {
            //Debug.Log("finger print removed! cleaning complete!!");
            
            isErasing = false;
            fingerPrint.GetComponent<Renderer>().material = cleanMat;

            gameController.TaskComplete(CleanerTask.CLEANWEAPON);
            gameController.BackTomainGame();
        }
    }

    public void Erasing(bool _erasing)
    {
        
        if (_erasing)
        {
            cameraTransform.DOMoveY(10, 0.01f);
            camera.enabled = true;
            cameraTransform.DOMoveY(5, 0.5f).SetEase(Ease.OutSine).OnComplete(()=> {
                isErasing = _erasing;
            });
        }
        else
        {
            isErasing = _erasing;
            cameraTransform.DOMoveY(10, 0.5f).SetEase(Ease.OutSine).OnComplete(() => {
                camera.enabled = false;
            });
        }
    }
}
