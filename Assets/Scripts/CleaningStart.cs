using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleaningStart : Interactable
{
    [Header("Weapons :")]
    [SerializeField] MeshRenderer rend;
    [SerializeField] Material cleanMat;
    [SerializeField] ParticleSystem glowParticle;
    void Start()
    {
        gameController = GameController.GetController();
    }

    public override void Interact()
    {
        base.Interact();
        if (otherCollider.GetComponent<PlayerController>().IsOccupied())
            return;

        gameController.FingerPrintMinigame();
        transform.GetComponent<Collider>().enabled = false;
        rend.material = cleanMat;
        glowParticle.Stop();
    }
}
