using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtyFloor : MonoBehaviour
{
    [SerializeField] Texture2D copyTex;
    Renderer rend;
    void Start()
    {
        
        rend = GetComponent<MeshRenderer>();
        Texture2D tex = rend.material.mainTexture as Texture2D;
        copyTex = new Texture2D(tex.width, tex.height);
        copyTex.SetPixels(tex.GetPixels());
        
        copyTex.Apply();
        rend.material.mainTexture = copyTex;
    }
}
