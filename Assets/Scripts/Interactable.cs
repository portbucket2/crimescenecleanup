using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    readonly string PLAYER = "Player";

    public Collider otherCollider;
    public GameController gameController;
    void Start()
    {
        gameController = GameController.GetController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            otherCollider = other;
            Interact();
           // other.GetComponent<PlayerController>().PickUp(this.transform);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(PLAYER))
        {
            otherCollider = other;
            Extract();
            // other.GetComponent<PlayerController>().PickUp(this.transform);
        }
    }
    public virtual void Interact()
    {

    }
    public virtual void Extract()
    {

    }

}
