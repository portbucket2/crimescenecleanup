using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOffInteractable : Interactable
{
    [SerializeField] ParticleSystem followUpParticle;
    [SerializeField] Animator doorAnimator;
    void Start()
    {
        
    }

    public override void Interact()
    {
        base.Interact();
        otherCollider.GetComponent<PlayerController>().DropOff(transform);
        if (followUpParticle)
            followUpParticle.Stop();

        if (doorAnimator)
            doorAnimator.SetTrigger("open");


        Debug.Log("dropped off");
    }
}
