using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagDollComponents : MonoBehaviour
{
    [SerializeField] Transform targetTransform;
    [SerializeField] float massScale;
    [SerializeField] List<Rigidbody> rbs;
    private void Awake()
    {
        rbs = new List<Rigidbody>();
        GetChildComponent(targetTransform);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void GetChildComponent(Transform tr)
    {
        int max = tr.childCount;
        for (int i = 0; i < max; i++)
        {
            if (tr.GetChild(i).GetComponent<Rigidbody>())
            {
                Rigidbody rb = tr.GetChild(i).GetComponent<Rigidbody>();
                //rb.mass = massScale;
                //rb.interpolation = RigidbodyInterpolation.None;
                rbs.Add(rb);
            }
            if (tr.childCount > 0)
            {
                GetChildComponent(tr.GetChild(i));
            }
        }
    }
    public List<Rigidbody> GetRigidbodies()
    {
        return rbs;
    }
}
