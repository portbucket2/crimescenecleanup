using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TaskParentUI : MonoBehaviour
{
    [Header("image componenets")]
    [SerializeField] Image parentImage;
    [SerializeField] Image childImage;

    [Header("state images")]
    [SerializeField] Sprite incompleteImage;
    [SerializeField] Sprite completeImage;
    [SerializeField] TextMeshProUGUI iconText;
    [Header("task images")]
    [SerializeField] Sprite removeBody;
    [SerializeField] Sprite cleanWeapon;
    [SerializeField] Sprite cleanBlood;
    [SerializeField] Sprite unlockDoor;
    [SerializeField] Sprite exitDoor;
    [Header("debug")]
    [SerializeField] CleanerTask currentTask;
    [SerializeField] Dictionary<CleanerTask, Sprite> taskImage;
    [SerializeField] Dictionary<CleanerTask, string> taskText;

    private void Awake()
    {
        taskImage = new Dictionary<CleanerTask, Sprite>();
        taskImage.Add(CleanerTask.REMOVEBODY, removeBody);
        taskImage.Add(CleanerTask.CLEANWEAPON, cleanWeapon);
        taskImage.Add(CleanerTask.CLEANBLOOD, cleanBlood);
        taskImage.Add(CleanerTask.UNLOCKDOOR, unlockDoor);

        taskText = new Dictionary<CleanerTask, string>();
        taskText.Add(CleanerTask.REMOVEBODY, "DISPOSE");
        taskText.Add(CleanerTask.CLEANWEAPON, "CLEAN");
        taskText.Add(CleanerTask.CLEANBLOOD, "MOP");
        taskText.Add(CleanerTask.UNLOCKDOOR, "UNLOCK");
    }
    void Start()
    {
        
    }

    public void SetTaskImage(CleanerTask _task)
    {
        parentImage.sprite = incompleteImage;
        childImage.sprite = taskImage[_task];
        iconText.text = taskText[_task];
        currentTask = _task;
    }
    public void SetExitDoor()
    {
        parentImage.sprite = incompleteImage;
        childImage.sprite = exitDoor;
        iconText.text = "EXIT";
        currentTask = CleanerTask.UNLOCKDOOR;
    }
    public bool CompletedTaskCheck(CleanerTask _taskChk)
    {
        bool isComplete = false;
        if (currentTask == _taskChk)
        {
            isComplete = true;
            Completed();
        }
        else
        {
            isComplete = false;
        }
        return isComplete;
    }
    public CleanerTask GetCleanerTask()
    {
        return currentTask;
    }
    void Completed()
    {
        parentImage.sprite = completeImage;
    }
}
