using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBody : MonoBehaviour
{
    [SerializeField] public Transform targetHand;
    [SerializeField] public Transform targetHip;
    [Header("Debug")]
    [SerializeField] List<Rigidbody> rbs;
    RagDollComponents rag;

    void Start()
    {
        rag = GetComponent<RagDollComponents>();
        rbs = rag.GetRigidbodies();
        int max = rbs.Count;
        for (int i = 0; i < max; i++)
        {
            rbs[i].mass = 0.5f;
        }
    }

    public void Dead()
    {
        int max = rbs.Count;
        for (int i = 0; i < max; i++)
        {
            rbs[i].isKinematic = true;
        }
        gameObject.SetActive(false);
    }
}
