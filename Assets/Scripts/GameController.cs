using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    private void Awake()
    {
        gameController = this;
    }
    public static GameController GetController() { return gameController; }

    [SerializeField] InputController inputController;
    [SerializeField] PlayerController playerController;
    [SerializeField] Camera mainCamera;
    [SerializeField] UIController uiController;
    [SerializeField] TaskMaster taskMaster;
    [SerializeField] FingerPrintCleaner cleaner;
    [SerializeField] LevelEndDoor levelEndDoor;
    [Header("virtual cameras")]
    [SerializeField] CinemachineBrain brain;
    [SerializeField] CinemachineVirtualCamera vCam1;
    [Header("Effects")]
    [SerializeField] ParticleSystem greenSplash;

    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);

    void Start()
    {
        
    }

    public InputController GetInputController() { return inputController; }
    public PlayerController GetPlayerController() { return playerController; }
    public Camera GetMainCamera() { return mainCamera; }
    public UIController GetUI() { return uiController; }
    public TaskMaster GetTaskMaster() { return taskMaster; }
    public Joystick GetJoystick() { return uiController.GetJoystick(); }
    public void GreenSplash() {
        if(greenSplash)
            greenSplash.Play();
    }
    public void LevelComplete() { uiController.LevelComplete(); }
    public void TaskComplete(CleanerTask _task) { taskMaster.TaskComplete(_task); }
    public void FingerPrintMinigame()
    {
        inputController.InputEnabled(false);
        cleaner.Erasing(true);
    }
    public void BackTomainGame()
    {
        inputController.InputEnabled(true);
        cleaner.Erasing(false);
    }
    public void AllTasksComplete()
    {
        levelEndDoor.OpenGate();
        playerController.FinalPath(levelEndDoor.transform);
    }
    public void LevelFailed()
    {
        StartCoroutine(LevelFailedRoutine());
        
    }
    IEnumerator LevelFailedRoutine()
    {
        playerController.Die();
        yield return WAITHALF;
        uiController.LevelFailed();
    }
}
