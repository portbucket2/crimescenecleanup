using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    [Header("Pick Up: ")]
    [SerializeField] FixedJoint grabber;
    [SerializeField] Transform toolPosition;
    [SerializeField] GameObject mop;
    [SerializeField] Animator animator;
    [SerializeField] float jumpPower;
    [Range(0f, 1f)]
    [SerializeField] float cleanEndPercentage;

    [Header("Debug :")]
    [SerializeField] bool isOccupied = false;
    [SerializeField] bool isMopping = false;
    [SerializeField] Rigidbody grabbedBody;
    [SerializeField] DeadBody dd;
    [SerializeField] Transform currentTool;
    [SerializeField] Color aColor = new Color(255, 255, 255, 0);
    [SerializeField] int blockSize;
    [SerializeField] ToolHolder toolHolder;
    [SerializeField] PathFinder pathFinder;
    [HideInInspector]
    [SerializeField] Color[] colors;

    
    NavMeshAgent agent;
    Camera mainCam;
    GameController gameController;
    GameManager gameManager;
    AnalyticsController analytics;

    readonly string DIRT = "dirt";
    readonly string IDLE = "idle";
    readonly string MOPPING = "mopping";
    readonly string SPEED = "speed";

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    void Start()
    {
        gameManager = GameManager.GetManager();
        analytics = AnalyticsController.GetController();

        pathFinder = GetComponent<PathFinder>();
        grabber.connectedBody = null;
        gameController = GameController.GetController();
        mainCam = gameController.GetMainCamera();
        int max = blockSize * blockSize;
        colors = new Color[max];
        for (int i = 0; i < max; i++)
        {
            colors[i] = aColor;
        }
        analytics.LevelStarted();
        //blockSize = (int)Mathf.Sqrt(colors.Length);
    }

    public void Moveable()
    {
        agent.isStopped = false;
    }
    public void Move(float hor, float ver)
    {        
        Vector3 temp = new Vector3(hor, 0f, ver);
        agent.SetDestination(transform.position + temp);
    }
    public void Stop()
    {
        agent.SetDestination(transform.position);
        agent.isStopped = true;
    }

    public void PickUp(Transform _obj, Transform targetDestination)
    {
        if (isMopping)
            return;

        isOccupied = true;
        gameController.GetUI().GetTutorial().ShowTut2();
        if (_obj.TryGetComponent<DeadBody>(out dd))
        {
            dd.targetHand.DOMove(grabber.transform.position, 0.01f).SetEase(Ease.OutSine).OnComplete(() => {
                grabbedBody = dd.targetHand.GetComponent<Rigidbody>();
                grabber.connectedBody = grabbedBody;
                grabbedBody.isKinematic = false;
                pathFinder.ShowPath(targetDestination);
            });
        }
        else
        {
            _obj.DOMove(grabber.transform.position, 0.2f).SetEase(Ease.OutSine).OnComplete(() => {
                grabbedBody = _obj.GetComponent<Rigidbody>();
                grabber.connectedBody = grabbedBody;
                grabbedBody.isKinematic = false;
            });
        }        
    }
    public void DropOff(Transform _obj)
    {
        if (isOccupied && grabbedBody)
        {
            gameController.GetUI().GetTutorial().StopTutorial();
            pathFinder.Resetpath();
            grabbedBody.isKinematic = true;
            grabbedBody.GetComponent<Collider>().enabled = false;
            grabber.connectedBody = null;
            grabbedBody.transform.DOJump(_obj.position, jumpPower, 1, 1f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                Transform body = grabbedBody.transform;
                grabbedBody = null;
                gameController.GreenSplash();
                isOccupied = false;
                gameController.TaskComplete(CleanerTask.REMOVEBODY);
                body.DOMoveY(-5, 0.5f).OnComplete(()=> {
                    if (dd)
                    {
                        dd.Dead();
                    }
                    dd = null;
                });
            });            
        }
    }
    public void DropBody(Transform _obj)
    {
        if (isOccupied && grabbedBody)
        {
            //grabbedBody.isKinematic = true;
            //grabbedBody.GetComponent<Collider>().enabled = false;

            gameController.GetUI().GetTutorial().ShowTut1();
            grabber.connectedBody = null;
            pathFinder.Resetpath();
            grabbedBody.transform.DOJump(_obj.position, jumpPower, 1, 1f).SetEase(Ease.InOutSine).OnComplete(() =>
            {
                Transform body = grabbedBody.transform;
                grabbedBody = null;
                //gameController.GreenSplash();
                isOccupied = false;
                //gameController.TaskComplete(CleanerTask.REMOVEBODY);
                //body.DOMoveY(-5, 0.5f).OnComplete(() => {
                //    if (dd)
                //    {
                //        dd.Dead();
                //    }
                //    dd = null;
                //});
            });
        }
    }
    public bool IsOccupied() { return isOccupied || isMopping; }
    public bool IsMoppingOnly() { return isMopping; }
    public bool IsOccupiedOnly() { return isOccupied; }
    public void PickUpTool(Transform _tool, ToolHolder _holder)
    {
        if (isOccupied)
            return;

        animator.SetTrigger(MOPPING);
        toolHolder = _holder;
        currentTool = _tool;
        mop.SetActive(true);
        //float yPos = _tool.position.y - toolPosition.position.y;
        //currentTool.SetParent(toolPosition);
        //currentTool.localPosition = new Vector3(0f, yPos, 0f);
        isMopping = true;
        pathFinder.ShowPath(_holder.FollowUpTransform());

    }
    public Transform DropTool()
    {
        animator.SetTrigger(IDLE);
        isMopping = false;
        if (currentTool) { 
            isMopping = false;
            toolHolder = null;
            mop.SetActive(false);
            return currentTool;
        }
        else
            return null;
    }

    private void Update()
    {
        if (isMopping)
        {
            animator.SetFloat(SPEED, agent.velocity.magnitude);
        }
        else
        {
            animator.SetFloat(SPEED, agent.velocity.magnitude);
        }
        
        if (isMopping)
        {
            Ray ray = new Ray(toolPosition.position,Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(ray,out hit,200f))
            {
                if (hit.transform.CompareTag(DIRT))
                {
                    Debug.DrawLine(ray.origin, hit.point, Color.green);
                    Renderer rend = hit.transform.GetComponent<Renderer>();
                    MeshCollider meshCollider = hit.collider as MeshCollider;

                    if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
                        return;

                    Texture2D tex = rend.material.mainTexture as Texture2D;
                    Vector2 pixelUV = hit.textureCoord;
                    pixelUV.x *= tex.width;
                    pixelUV.y *= tex.height;

                    tex.SetPixels((int)pixelUV.x, (int)pixelUV.y, blockSize, blockSize, colors);

                    CheckCleningPercentage(tex, hit.transform.gameObject);
                    //tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, aColor);
                    tex.Apply();
                }
            }
        }
    }

    void CheckCleningPercentage(Texture2D _tex, GameObject dirtyFloor)
    {
        Color[] colors = _tex.GetPixels(0);
        int max = colors.Length;
        int count = 0;
        for (int i = 0; i < max; i++)
        {
            if (colors[i].a <= 0.1)
                count++;
        }
        float perc = (float)count / max;
        Debug.Log("dirty floor Perc: " + perc);
        if (perc > cleanEndPercentage)
        {
            //Debug.Log("level Complete!");
            toolHolder.ReGlow();
            gameController.TaskComplete(CleanerTask.CLEANBLOOD);
            dirtyFloor.SetActive(false);
            pathFinder.Resetpath();
        }
    }

    public void Die()
    {
        gameController.GetInputController().InputEnabled(false);
        animator.enabled = false;
        GetComponent<Collider>().enabled = false;
        analytics.LevelFailed();
        if (isOccupied && grabbedBody)
        {
            grabbedBody.isKinematic = true;
            grabbedBody.GetComponent<Collider>().enabled = false;
            grabber.connectedBody = null;

            grabbedBody = null;
            gameController.GreenSplash();
            isOccupied = false;
            if (dd)
            {
                dd.Dead();
            }
            dd = null;
        }
    }
    public void FinalPath(Transform gate)
    {
        pathFinder.ShowPath(gate);
    }
}
